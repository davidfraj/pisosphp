<?php 
// Fichero  models/class.pisosModel.php
require('models/class.pisoModel.php');
class PisosModel{

	private $elementos;
	private $conexion;

	public function __construct(){
		$this->elementos=[];
		$this->conexion=Conexion::$conexion;
	}

	public function listado($numregistrosporpagina, $numpag=0){
		
		$inicio=$numregistrosporpagina*$numpag;

		$sql="SELECT * FROM pisos LIMIT $inicio,$numregistrosporpagina";
		$consulta=$this->conexion->query($sql);
		while($fila=$consulta->fetch_array()){
			$this->elementos[]=new pisoModel($fila);
		}
		return $this->elementos;
	}

	public function numPaginas($numregistrosporpagina=1){
		$sql="SELECT * FROM pisos";
		$consulta=$this->conexion->query($sql);
		return ceil($consulta->num_rows/$numregistrosporpagina);
	}

	public function detalle($id){
		$sql="SELECT * FROM pisos WHERE idPiso=$id";
		$consulta=$this->conexion->query($sql);
		$fila=$consulta->fetch_array();
		return new pisoModel($fila);
	}

	public function insertar($direccionPiso, $caracteristicasPiso, $precioPiso, $ciudadPiso, $imagenPiso){

		$sql="INSERT INTO pisos(direccionPiso, caracteristicasPiso, precioPiso, ciudadPiso, imagenPiso)VALUES('$direccionPiso', '$caracteristicasPiso', '$precioPiso', '$ciudadPiso', '$imagenPiso')";

		if($this->conexion->query($sql)==true){
			return true;
		}else{
			return $this->conexion->error;
		}

	}

	public function modificar($idPiso, $direccionPiso, $caracteristicasPiso, $precioPiso, $ciudadPiso, $imagenPiso){

		$sql="UPDATE pisos SET direccionPiso='$direccionPiso', caracteristicasPiso='$caracteristicasPiso', precioPiso='$precioPiso', ciudadPiso='$ciudadPiso', imagenPiso='$imagenPiso' WHERE idPiso='$idPiso'";

		if($this->conexion->query($sql)==true){
			return true;
		}else{
			return $this->conexion->error;
		}

	}

	public function borrar($id){
		$sql="DELETE FROM pisos WHERE idPiso=$id";
		if($this->conexion->query($sql)==true){
			return true;
		}else{
			return $this->conexion->error;
		}
	}

}


?>
