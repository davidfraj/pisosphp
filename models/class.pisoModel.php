<?php 
// Fichero  models/class.pisoModel.php

class PisoModel{
 	private $idPiso;
 	private $direccionPiso;
	private $caracteristicasPiso;
	private $imagenPiso;
	private $precioPiso;
	private $ciudadPiso;

	public function __construct($fila){
		$this->idPiso=$fila['idPiso'];
		$this->direccionPiso=$fila['direccionPiso'];
		$this->caracteristicasPiso=$fila['caracteristicasPiso'];
		$this->imagenPiso=$fila['imagenPiso'];
		$this->precioPiso=$fila['precioPiso'];
		$this->ciudadPiso=$fila['ciudadPiso'];
	}

    /**
     * Gets the value of idPiso.
     *
     * @return mixed
     */
    public function getIdPiso()
    {
        return $this->idPiso;
    }

    /**
     * Sets the value of idPiso.
     *
     * @param mixed $idPiso the id piso
     *
     * @return self
     */
    private function _setIdPiso($idPiso)
    {
        $this->idPiso = $idPiso;

        return $this;
    }

    /**
     * Gets the value of direccionPiso.
     *
     * @return mixed
     */
    public function getDireccionPiso()
    {
        return $this->direccionPiso;
    }

    /**
     * Sets the value of direccionPiso.
     *
     * @param mixed $direccionPiso the direccion piso
     *
     * @return self
     */
    private function _setDireccionPiso($direccionPiso)
    {
        $this->direccionPiso = $direccionPiso;

        return $this;
    }

    /**
     * Gets the value of caracteristicasPiso.
     *
     * @return mixed
     */
    public function getCaracteristicasPiso()
    {
        return $this->caracteristicasPiso;
    }

    /**
     * Sets the value of caracteristicasPiso.
     *
     * @param mixed $caracteristicasPiso the caracteristicas piso
     *
     * @return self
     */
    private function _setCaracteristicasPiso($caracteristicasPiso)
    {
        $this->caracteristicasPiso = $caracteristicasPiso;

        return $this;
    }

    /**
     * Gets the value of imagenPiso.
     *
     * @return mixed
     */
    public function getImagenPiso()
    {
        return $this->imagenPiso;
    }

    /**
     * Sets the value of imagenPiso.
     *
     * @param mixed $imagenPiso the imagen piso
     *
     * @return self
     */
    private function _setImagenPiso($imagenPiso)
    {
        $this->imagenPiso = $imagenPiso;

        return $this;
    }

    /**
     * Gets the value of precioPiso.
     *
     * @return mixed
     */
    public function getPrecioPiso()
    {
        return $this->precioPiso;
    }

    /**
     * Sets the value of precioPiso.
     *
     * @param mixed $precioPiso the precio piso
     *
     * @return self
     */
    private function _setPrecioPiso($precioPiso)
    {
        $this->precioPiso = $precioPiso;

        return $this;
    }

    /**
     * Gets the value of ciudadPiso.
     *
     * @return mixed
     */
    public function getCiudadPiso()
    {
        return $this->ciudadPiso;
    }

    /**
     * Sets the value of ciudadPiso.
     *
     * @param mixed $ciudadPiso the ciudad piso
     *
     * @return self
     */
    private function _setCiudadPiso($ciudadPiso)
    {
        $this->ciudadPiso = $ciudadPiso;

        return $this;
    }
}



?>