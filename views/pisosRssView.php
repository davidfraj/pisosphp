<?php 
header("Content-type: text/xml");
$xml = new SimpleXMLElement('<xml/>');
$xml->addAttribute('version', '1.0');
$rss = $xml->addChild('rss');
$rss->addAttribute('version', '2.0');
?>

<rss version="2.0"
xmlns:content="http://purl.org/rss/1.0/modules/content/"
	xmlns:wfw="http://wellformedweb.org/CommentAPI/"
xmlns:dc="http://purl.org/dc/elements/1.1/"
xmlns:atom="http://www.w3.org/2005/Atom"
>
	<channel>
	<title><![CDATA[RSS :: Pisos de David Fraj Blesa]]></title>
	<link>http://192.168.1.58/datw/David/pisosphp/</link>
	<description>RSS :: Pisos de David Fraj Blesa</description>
	<language>es</language>
	<pubDate>Tue, 20 Jun 2017 17:51:24 +0200</pubDate>
	
	<?php foreach ($elementos as $piso) { ?>
	<item>
		<title><![CDATA[<?php echo $piso->getDireccionPiso(); ?>]]></title>
		<pubDate>Tue, 20 Jun 2017 06:00:00 +0200</pubDate>

		<category><?php echo $piso->getCiudadPiso(); ?></category>

		<link>http://192.168.1.58/datw/David/pisosphp/index.php?controller=pisosController.php&amp;accion=detalle&amp;id=<?php echo $piso->getIdPiso(); ?></link>
		<description><![CDATA[<?php echo $piso->getCaracteristicasPiso(); ?>]]></description>
	</item>
	<?php } ?>
	</channel>
</rss>