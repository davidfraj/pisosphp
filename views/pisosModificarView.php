<?php require('views/encabezadoView.php'); ?>

      <div class="row">
        <section class="col-sm-12">
            <?php  
            
echo Form::inicio('index.php?controller=pisosController.php&accion=modificacion');
echo Form::label('Direccion del piso:');
echo Form::texto('direccionPiso', $e->getDireccionPiso());
echo Form::label('Caracteristicas del piso:');
echo Form::areatexto('caracteristicasPiso', $e->getCaracteristicasPiso());
echo Form::label('Imagen del piso:');
echo Form::fichero('imagenPiso', $e->getImagenPiso());
echo Form::label('Precio del piso:');
echo Form::texto('precioPiso', $e->getPrecioPiso());
echo Form::label('Ciudad del piso:');
echo Form::texto('ciudadPiso', $e->getCiudadPiso());
echo Form::hidden('idPiso', $e->getIdPiso());
echo Form::hidden('imagenPisoAnt', $e->getImagenPiso());
echo Form::submit('Guardar cambios');
echo Form::fin();
            
            ?>
        </section>
      </div>

<?php require('views/pieView.php'); ?> 