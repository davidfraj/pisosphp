<?php require('views/encabezadoView.php'); ?>

      <div class="row">
        <section class="col-sm-12">
          
           
            <article>
              <header>
                  <h2><?php echo $e->getDireccionPiso(); ?>
                  - <small><?php echo $e->getCiudadPiso(); ?></small>
                  - <a href="index.php?controller=pisosController.php">Volver</a>
                  </h2>

                  Acciones: 
                  <a href="index.php?controller=pisosController.php&accion=borrar&id=<?php echo $e->getIdPiso();?>"><span class="glyphicon glyphicon-remove-circle"></span></a>

              </header>
              <section class="clearfix">
                <img src="imagenes/<?php echo $e->getImagenPiso(); ?>" width="200" style="float:left; padding: 10px; border-radius: 20px;">
                <?php echo $e->getCaracteristicasPiso(); ?>
              </section>
              <footer><?php echo $e->getPrecioPiso(); ?></footer>
            </article>
            <hr>
            
            <?php cargarModulo('comentarios'); ?>

        </section>
      </div>

<?php require('views/pieView.php'); ?> 