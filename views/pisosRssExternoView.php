<?php require('views/encabezadoView.php'); ?>
      <div class="row">
        <section class="col-sm-12">
            <?php foreach ($elementos as $e) { ?>
            <article>
              <a href="<?php echo $e->link; ?>">
              <h2>
                <?php echo $e->title; ?>
                <small><?php echo $e->title; ?></small>
              </h2>
              </a>
              <section>
                <?php echo $e->description; ?>
              </section>
            </article>
            <hr>
            <?php } ?>
        </section>
      </div>
<?php require('views/pieView.php'); ?> 