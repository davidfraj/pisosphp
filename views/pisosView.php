<?php require('views/encabezadoView.php'); ?>

      
      <div class="row">
        <nav class="col-sm-4">
          
          <?php cargarModulo('menu'); ?>
          <br>
          <?php cargarModulo('banner'); ?>

        </nav>
        <section class="col-sm-8">
          
            <?php foreach ($elementos as $e) { ?>
            <article>
              <header>
                <a href="index.php?controller=pisosController.php&accion=detalle&id=<?php echo $e->getIdPiso();?>">
                  <h2><?php echo $e->getDireccionPiso(); ?>
                  - <small><?php echo $e->getCiudadPiso(); ?></small>
                  </h2>
                </a>
                Acciones: 
                <a href="index.php?controller=pisosController.php&accion=borrar&id=<?php echo $e->getIdPiso();?>"><span class="glyphicon glyphicon-remove-circle"></span></a>

                <a href="index.php?controller=pisosController.php&accion=modificar&id=<?php echo $e->getIdPiso();?>"><span class="glyphicon glyphicon-scissors"></span></a>

                <?php cargarModulo('votar'); ?>

              </header>
              <section class="clearfix">
                <img src="imagenes/<?php echo $e->getImagenPiso(); ?>" width="200" style="float:left; padding: 10px; border-radius: 20px;">
                <?php echo $e->getCaracteristicasPiso(); ?>
              </section>
              <footer><?php echo $e->getPrecioPiso(); ?></footer>
            </article>
            <hr>
            <?php } ?>


            <?php  
            // $numpag,viene del controlador
            // $numPaginas, viene del controlador, con el numero total
            // de paginas que queremos mostrar
            echo Helper::pagination($numpag, $numPaginas, 'index.php?controller=pisosController.php&numpag=');
            ?>

            



        </section>
      </div>

    

<?php require('views/pieView.php'); ?> 