<?php require('views/encabezadoView.php'); ?>

      <div class="row">
        <section class="col-sm-12">
            <?php  
            echo Form::inicio('index.php?controller=pisosController.php&accion=insercion');
            echo Form::label('Direccion del piso:');
            echo Form::texto('direccionPiso');
            echo Form::label('Caracteristicas del piso:');
            echo Form::areatexto('caracteristicasPiso');
            echo Form::label('Imagen del piso:');
            echo Form::fichero('imagenPiso');
            echo Form::label('Precio del piso:');
            echo Form::texto('precioPiso');
            echo Form::label('Ciudad del piso:');
            echo Form::texto('ciudadPiso');
            echo Form::submit('Insertar');
            echo Form::fin();
            ?>
        </section>
      </div>

<?php require('views/pieView.php'); ?> 