<?php  


    /**
    * Archivo class/class.helper.php
    */

    class Helper 
    {
        
        public static function pagination($numpag,$numPag,$url)
        {
            $r = '';

            //-- Paginacion --

            $r.= '<ul class="pagination">';
            
            //-- pagina anterior -->
           

            if($numpag == 0){
              $c = 'disabled';
              $numpaganterior = 0;
            }
            else{
              $c = '';
              $numpaganterior = $numpag-1;

            } 
         
            $r.= '<li class="'.$c.'"><a href="'.$url.$numpaganterior.'">&laquo;</a></li>';

            //-- /pagina anterior -->

            //-- Genero los indices de pagina ---

            for($i=0; $i<$numPag; $i++){
                  
                if($i==$numpag){
                    $c= 'active';
                }
                else{
                    $c='';
                }
          
                $r.= '<li class="'.$c.'"><a href="'.$url.$i.'">'.($i+1).'</a></li>';

            } 

            //-- /Genero los indices de pagina ---
          
            //-- pagina posterior --
           

            if($numpag < $numPag-1){
              $c = '';
              $numpagposterior = $numpag+1;
              
            }
            else{
              $c = 'disabled';
              $numpagposterior = $numPag-1;

            } 

            $r.='<li class="'.$c.'"><a href="'.$url.$numpagposterior.'">&raquo;</a></li>';
            //-- /pagina posterior -->

            $r.='</ul>';

           
            return $r;   
        }
    }
