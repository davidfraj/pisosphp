<?php  
//Fichero classes/class.form.php

class Form{

	public static function inicio($action,$class='form-vertical'){
		return '<form method="POST" action="'.$action.'" role="form" class="'.$class.'" enctype="multipart/form-data">';
	}

	public static function fin(){
		return '</form>';
	}

	public static function texto($nombre, $valor=''){
		return '<input type="text" name="'.$nombre.'" value="'.$valor.'" class="form-control">';
	}

	public static function password($nombre, $valor=''){
		return '<input type="password" name="'.$nombre.'" value="'.$valor.'" class="form-control">';
	}

	public static function fichero($nombre, $value=''){
		return '<img src="imagenes/'.$value.'" width="200"><input type="file" name="'.$nombre.'" class="form-control">';
	}

	public static function submit($nombre){
		return '<input type="submit" name="'.$nombre.'" value="'.$nombre.'" class="form-control">';
	}

	public static function hidden($nombre, $valor=''){
		return '<input type="hidden" name="'.$nombre.'" value="'.$valor.'" class="form-control">';
	}

	public static function areatexto($nombre, $valor=''){
		return '<textarea name="'.$nombre.'" class="form-control">'.$valor.'</textarea>';
	}

	public static function label($nombre){
		return '<label>'.$nombre.'</label>';
	}

	public static function checkbox($nombre){
		return '<input type="checkbox" name="'.$nombre.'"class="form-control" checked>';
	}

}

?>