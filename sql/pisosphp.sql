-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 04-07-2017 a las 15:18:08
-- Versión del servidor: 10.1.19-MariaDB
-- Versión de PHP: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `pisosphp`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentarios`
--

CREATE TABLE `comentarios` (
  `idCom` int(11) NOT NULL,
  `nombreCom` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `textoCom` longtext COLLATE utf8_spanish_ci NOT NULL,
  `fechaCom` datetime NOT NULL,
  `idUsu` int(11) NOT NULL,
  `idPiso` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `comentarios`
--

INSERT INTO `comentarios` (`idCom`, `nombreCom`, `textoCom`, `fechaCom`, `idUsu`, `idPiso`) VALUES
(1, 'comentario 12', 'comentario 12', '2017-07-03 00:00:00', 1, 2),
(2, 'comentario 2', 'comentario 2', '2017-07-03 00:00:00', 1, 2),
(3, 'comentario 3', 'comentario 3', '2017-07-03 00:00:00', 1, 2),
(4, 'comentario 4', 'comentario 4', '2017-07-03 00:00:00', 1, 3),
(6, 'Comentario 10', 'Texto de comentario 10', '2017-07-03 16:56:45', 2, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pisos`
--

CREATE TABLE `pisos` (
  `idPiso` int(11) NOT NULL,
  `direccionPiso` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `caracteristicasPiso` longtext COLLATE utf8_spanish_ci NOT NULL,
  `imagenPiso` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `precioPiso` float NOT NULL,
  `ciudadPiso` tinytext COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `pisos`
--

INSERT INTO `pisos` (`idPiso`, `direccionPiso`, `caracteristicasPiso`, `imagenPiso`, `precioPiso`, `ciudadPiso`) VALUES
(2, 'Plaza Roma', ' Los pisos industriales son estructuras de concreto con características muy específicas para garantizar un comportamiento que permita desarrollar sobre éstas diferentes procesos en condiciones de servicio.<br><br>\r\n \r\nUn piso industrial es una superfic', 'piso2.jpg', 220000, 'Zaragoza'),
(3, 'Escoriaza y Fabro 16', ' A continuación le presentamos a Concretos Poliméricos de México S.A. de C.V., proveedor de pisos industriales:<br><br>\r\n \r\nConcretos Poliméricos de México S.A. de C.V., es el fabricante de Concreto Polimérico prefabricado para el sector de la construcción.<br><br>\r\n \r\nInició con paneles para fachadas y actualmente su línea de fabricación de prefabricados es amplia y de acuerdo a los proyectos de sus clientes, con el compromiso de mejorar continuamente sus procesos, productos y servicios, lo que los ha llevado a ser una empresa versátil.<br><br>\r\n \r\nConozca el Perfil, Productos, Dirección y Teléfono de Concretos Poliméricos de México S.A. de C.V.<br><br>\r\n \r\nO bien, haga contacto directo con Concretos Poliméricos de México S.A. de C.V., para solicitar mayor información sobre sus pisos industriales.<br><br>', 'piso3.jpg', 190000, 'zaragoza'),
(4, 'Su casa de ensueño 19', 'Los pisos flotantes son uno de los revestimientos preferidos por quienes están construyendo su hogar familiar. Son cálidos, fáciles de mantener y aportan una elegancia única a cualquier ambiente. Aunque su costo es algo elevado, bien vale la pena cuando tomas en consideración su durabilidad, siempre que esté colocado por manos experimentadas en ello.<br><br> \r\n\r\nSe trata de un revestimiento de suelos en el que se colocan maderas encoladas entre sí, y por sobre diversas superficies secas, firmes y planas. Estos entablonados pueden colocarse sobre maderas existentes, carpetas y también sobre embaldosados, mosaicos y pisos cerámicos, reduciendo su costo de restauración ante daños y permitiendo un piso más cálido y llamativo.', 'piso5.jpg', 325000, 'Zaragoza');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `idUsu` int(11) NOT NULL,
  `nombreUsu` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `claveUsu` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `correoUsu` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `tipoUsu` tinytext COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`idUsu`, `nombreUsu`, `claveUsu`, `correoUsu`, `tipoUsu`) VALUES
(1, 'admin', '81dc9bdb52d04dc20036dbd8313ed055', 'admin@gmail.com', 'administrador'),
(2, 'invitado', '81dc9bdb52d04dc20036dbd8313ed055', 'invitado@gmail.com', 'normal');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `comentarios`
--
ALTER TABLE `comentarios`
  ADD PRIMARY KEY (`idCom`);

--
-- Indices de la tabla `pisos`
--
ALTER TABLE `pisos`
  ADD PRIMARY KEY (`idPiso`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idUsu`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `comentarios`
--
ALTER TABLE `comentarios`
  MODIFY `idCom` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `pisos`
--
ALTER TABLE `pisos`
  MODIFY `idPiso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `idUsu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
