<?php  
session_start();
require('funciones.php');
require('classes/class.conexion.php');
require('classes/class.form.php'); //Para formularios
require('classes/class.forma.php'); //Para formularios avanzados
require('classes/class.helper.php');

if(isset($_GET['controller'])){
	$controller=$_GET['controller'];
}else{
	$controller='pisosController.php';
}

require('controllers/'.$controller);

Conexion::$conexion->close();
?>