<?php 
//Fichero  modulos/menu/controller/menu.php

require('modulos/menu/model/menuModel.php');

$menu=new MenuModel();

$menu->add('index.php', 'Ir a Inicio');
$menu->add('index.php?controller=pisosController.php&accion=rssexterno', 'Pisos de otras webs');
$menu->add('concacto.php', 'Web de contacto');

$items=$menu->devolver();

require('modulos/menu/view/menuView.php');

?>