<?php  
//Fichero modulos/menu/model/itemMenuModel.php
class ItemMenuModel{
	private $enlace;
	private $titulo;
	public function __construct($enlace, $titulo){
		$this->enlace=$enlace;
		$this->titulo=$titulo;
	}
	public function getEnlace(){
		return $this->enlace;
	}
	public function getTitulo(){
		return $this->titulo;
	}
}
?>