<?php  
//Fichero modulos/menu/model/menuModel.php

//Llamamos a nuestro modelo de item, que sera, un
//elemento de menu
require('modulos/menu/model/itemMenuModel.php');
//Declaro el nombre de clase
class MenuModel{
	//Creamos un vector de elementos (del menu)
	private $items=[];
	//Creamos el metodo para añadir un elemento a nuestro menu
	public function add($enlace, $titulo){
		$item=new ItemMenuModel($enlace, $titulo);
		$this->items[]=$item;
	}
	//Creamos un metodo para devolver TODOS los elementos del menu
	public function devolver(){
		return $this->items;
	}
}
?>