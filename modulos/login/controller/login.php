<?php 
//Fichero  modulos/login/controller/login.php

require('modulos/login/model/usuarioModel.php');

$login=new UsuarioModel();

if(!isset($_SESSION['usuarioConectado'])){
	$_SESSION['usuarioConectado']=NULL;
}

if(isset($_COOKIE['mantener'])){
	$_SESSION['usuarioConectado']=$login->comprobarCookie($_COOKIE['mantener']);
	setcookie('mantener', md5($_SESSION['usuarioConectado']['correoUsu']), time()+(60*60*24*7));
}

if(isset($_POST['Conectar'])){
	$_SESSION['usuarioConectado']=$login->comprobar($_POST['nombreUsu'], $_POST['claveUsu']);
	if(isset($_POST['mantener'])){
		setcookie('mantener', md5($_SESSION['usuarioConectado']['correoUsu']), time()+(60*60*24*7));
	}
}

if(isset($_GET['desconectar'])){
	$_SESSION['usuarioConectado']=NULL;
	setcookie('mantener', '', 0);
}


require('modulos/login/view/loginView.php');

?>