<?php  
//fichero  modulos/banner/model/itemBannerModel.php
class ItemBannerModel{
		private $url;
		private $imagen;
		public function __construct($url, $imagen){
			$this->url=$url;
			$this->imagen=$imagen;
		}
		public function getUrl(){
			return $this->url;
		}
		public function getImagen(){
			return $this->imagen;
		}
}
?>
