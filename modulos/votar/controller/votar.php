<?php  
//Fichero  modulos/votar/controller/votar.php
require_once('modulos/votar/model/votosModel.php');
$votos=new Votos();

//llamo de alguna forma, al piso actual
global $e;

//Vamos a votar
if(isset($_GET['accionVotar'])){
	if($e->getIdPiso()==$_GET['idPiso']){
		$votos->votar($_GET['idUsu'], $_GET['idPiso'], $_GET['accionVotar']);
	}
}

//Extraemos el numero de estrellas de este piso
//Esta es la media redondeada de votos
$estrellas=round($votos->estrellas($e),0);

//Llamo a la vista
require('modulos/votar/view/votarView.php');
?>