<?php  
//Fichero modulos/comentarios/model/comentariosModel.php
require('modulos/comentarios/model/comentarioModel.php');
class Comentarios{
	private $conexion;
	private $elementos=[];

	public function __construct(){
		$this->conexion=Conexion::$conexion;
	}
	public function listado($idPiso){
		$sql="SELECT * FROM comentarios WHERE idPiso=$idPiso";
		$consulta=$this->conexion->query($sql);
		while($fila=$consulta->fetch_array()){
			$this->elementos[]=new Comentario($fila);
		}
		return $this->elementos;
	}

	public function insertar($nombreCom, $textoCom, $idPiso, $idUsu, $fechaCom){
		$sql="INSERT INTO comentarios(nombreCom, textoCom, idPiso, idUsu, fechaCom)VALUES('$nombreCom', '$textoCom', '$idPiso', '$idUsu', '$fechaCom')";
		$consulta=$this->conexion->query($sql);
	}

	public function borrar($idCom){
		$sql="DELETE FROM comentarios WHERE idCom=$idCom";
		$consulta=$this->conexion->query($sql);
	}

	public function modificar($idCom){
		$sql="SELECT * FROM comentarios WHERE idCom=$idCom";
		$consulta=$this->conexion->query($sql);
		return new Comentario($consulta->fetch_array());
	}

	public function modificacion($nombreCom, $textoCom, $idCom){
		$sql="UPDATE comentarios SET nombreCom='$nombreCom', textoCom='$textoCom' WHERE idCom=$idCom";
		$consulta=$this->conexion->query($sql);
	}
}

?>