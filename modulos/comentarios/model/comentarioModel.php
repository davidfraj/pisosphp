<?php  
//Fichero modulos/comentarios/model/comentarioModel.php
class Comentario{

	private $idCom;
	private $nombreCom;
	private $textoCom;
	private $fechaCom;
	private $idUsu;
	private $idPiso;
	private $nombreUsu;
	public $esEditable=false;
	public $esBorrable=false;

	public function __construct($fila){

		$this->idCom=$fila['idCom'];
		$this->nombreCom=$fila['nombreCom'];
		$this->textoCom=$fila['textoCom'];
		$this->fechaCom=$fila['fechaCom'];
		$this->idUsu=$fila['idUsu'];
		$this->idPiso=$fila['idPiso'];
		$sql="SELECT * FROM usuarios WHERE idUsu=".$this->idUsu;
		$consulta=Conexion::$conexion->query($sql);
		$this->nombreUsu=$consulta->fetch_array()['nombreUsu'];

		if($_SESSION['usuarioConectado']['tipoUsu']=='administrador'){
			$this->esBorrable=true;
		}

		if($_SESSION['usuarioConectado']['idUsu']==$fila['idUsu']){
			$this->esBorrable=true;
			$this->esEditable=true;
		}

	}


    /**
     * @return mixed
     */
    public function getIdCom()
    {
        return $this->idCom;
    }

    /**
     * @param mixed $idCom
     *
     * @return self
     */
    public function setIdCom($idCom)
    {
        $this->idCom = $idCom;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNombreCom()
    {
        return $this->nombreCom;
    }

    /**
     * @param mixed $nombreCom
     *
     * @return self
     */
    public function setNombreCom($nombreCom)
    {
        $this->nombreCom = $nombreCom;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTextoCom()
    {
        return $this->textoCom;
    }

    /**
     * @param mixed $textoCom
     *
     * @return self
     */
    public function setTextoCom($textoCom)
    {
        $this->textoCom = $textoCom;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFechaCom()
    {
        return $this->fechaCom;
    }

    /**
     * @param mixed $fechaCom
     *
     * @return self
     */
    public function setFechaCom($fechaCom)
    {
        $this->fechaCom = $fechaCom;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdUsu()
    {
        return $this->idUsu;
    }

    /**
     * @param mixed $idUsu
     *
     * @return self
     */
    public function setIdUsu($idUsu)
    {
        $this->idUsu = $idUsu;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdPiso()
    {
        return $this->idPiso;
    }

    /**
     * @param mixed $idPiso
     *
     * @return self
     */
    public function setIdPiso($idPiso)
    {
        $this->idPiso = $idPiso;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNombreUsu()
    {
        return $this->nombreUsu;
    }

    /**
     * @param mixed $nombreUsu
     *
     * @return self
     */
    public function setNombreUsu($nombreUsu)
    {
        $this->nombreUsu = $nombreUsu;

        return $this;
    }
}