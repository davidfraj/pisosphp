<?php  
//fichero  modulos/comentarios/controller/comentarios.php

//Hay que llamar al modelo
require('modulos/comentarios/model/comentariosModel.php');
$com=new Comentarios();

//Si le paso una accionCom, evaluo dicha accion, y la realizo a 
//traves del modelo
$verFormulario='insertar';

if(isset($_GET['accionCom'])){
	if($_GET['accionCom']=='borrar'){
		$com->borrar($_GET['idCom']);
	}
	if($_GET['accionCom']=='modificar'){
		$verFormulario='modificar';
		$comentarioMod=$com->modificar($_GET['idCom']);
	}
	if($_GET['accionCom']=='modificacion'){
		$nombreCom=$_POST['nombreCom'];
		$textoCom=$_POST['textoCom'];
		$idCom=$_POST['idCom'];
		$com->modificacion($nombreCom, $textoCom, $idCom);
	}
}

if(isset($_POST['Publicar'])){
	//Me las apaño para agregar un comentario a la bbdd
	$nombreCom=$_POST['nombreCom'];
	$textoCom=$_POST['textoCom'];
	$idPiso=$_GET['id'];
	$idUsu=$_SESSION['usuarioConectado']['idUsu'];
	$fechaCom=date('Y-m-d H:i:s');
	$com->insertar($nombreCom, $textoCom, $idPiso, $idUsu, $fechaCom);
}

//Le paso $_GET['id'], para saber los comentarios de ese piso
$comentarios=$com->listado($_GET['id']);

//Llamada a la vista
require('modulos/comentarios/view/comentariosView.php');

//Llamada a la vista
if($verFormulario=='insertar'){
require('modulos/comentarios/view/insertarComentarioView.php');
}else{
require('modulos/comentarios/view/modificarComentarioView.php');
}
?>