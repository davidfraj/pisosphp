<?php 
//Fichero modulos/comentarios/view/comentariosView.php
?>

<?php foreach ($comentarios as $c) { ?>
		
	<article>
		<header>
			<?php echo $c->getNombreCom(); ?> 
			<small>
				<?php echo $c->getNombreUsu(); ?>
				
				<?php if($c->esEditable){ ?>
					- <a href="index.php?controller=pisosController.php&accion=detalle&id=<?php echo $_GET['id'];?>&idCom=<?php echo $c->getIdCom();?>&accionCom=modificar">Modificar</a>
				<?php } ?>	
				<?php if($c->esBorrable){ ?>
					- <a href="index.php?controller=pisosController.php&accion=detalle&id=<?php echo $_GET['id'];?>&idCom=<?php echo $c->getIdCom();?>&accionCom=borrar">Eliminar</a>
				<?php } ?>
			</small>
		</header>
		<?php echo $c->getTextoCom(); ?>
		<footer><?php echo $c->getFechaCom(); ?></footer>
	</article><hr>

<?php } ?>




